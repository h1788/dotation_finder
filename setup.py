"""Setup file."""
from setuptools import find_packages, setup

setup(
    name="dotation_finder",
    version="0.0.1",
    author="Bards_AI",
    install_requires=[],
    packages=find_packages(),
)
