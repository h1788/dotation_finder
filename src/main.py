from flask import Blueprint, render_template

from src.web_app import create_app

main = Blueprint("main", __name__)


@main.route("/")
def index():
    return render_template("index.html")


@main.route("/new_company")
def new_company():
    return render_template("index_new_companies.html")


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0")
