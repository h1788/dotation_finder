from dataclasses import dataclass

import pandas as pd


@dataclass
class DotationsData:
    id: int
    nazwa_dotacji: str
    kwota: float
    cel: list
    otwarty: bool
    koniec_zgloszen: str
    poczatek_projektu: str
    zwrotna: bool
    equity: float
    konsorcjum: bool
    kategoria: list
    rodzaj: list
    url: str
    files: list
    wklad_wlasny: float


def read_dotations_data(category: list, typ: str):
    df = pd.read_csv("src/datasets/dotations.csv")
    rows = df.to_records(index=False)
    dotations_list = []
    if len(rows) == 0:
        return None
    else:
        for row in rows:
            dotations_list.append(
                DotationsData(
                    row["id"],
                    row["nazwa_dotacji"],
                    row["kwota"],
                    row["cel"].replace("[", "").replace("]", "").split(","),
                    row["otwarty"],
                    row["koniec_zgloszen"],
                    row["poczatek_projektu"],
                    row["zwrotna"],
                    row["equity"],
                    row["konsorcjum"],
                    row["kategoria"].replace("[", "").replace("]", "").replace('"', "").split(","),
                    row["rodzaj"].replace("[", "").replace("]", "").replace('"', "").split(","),
                    row["url"],
                    row["files"].replace("[(", "").replace("]", "").split("), ("),
                    row['wklad_wlasny'],
                )
            )
        result_list = []
        for element in dotations_list:
            for i in range(len(element.files)):
                element.files[i] = element.files[i].split(",")
            for item in category:
                if item in element.rodzaj and typ in element.kategoria:
                    result_list.append(element)
        return result_list
