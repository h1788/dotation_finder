"""
Sekcja A – Rolnictwo, leśnictwo, łowiectwo i rybactwo
Sekcja B – Górnictwo i wydobywanie
Sekcja C – Przetwórstwo przemysłowe
Sekcja D – wytwarzanie i zaopatrywanie w energię elektryczną, gaz, parę wodną, gorącą wodę i powietrze do układów klimatyzacyjnych
Sekcja E – dostawa wody; gospodarowanie ściekami i odpadami oraz działalność związana z rekultywacją
Sekcja F – Budownictwo
Sekcja G – Handel hurtowy i detaliczny; naprawa pojazdów samochodowych, włączając motocykle
Sekcja H – Transport i gospodarka magazynowa
Sekcja I – Działalność związana z zakwaterowaniem i usługami gastronomicznymi
Sekcja J – Informacja i komunikacja
Sekcja K – Działalność finansowa i ubezpieczeniowa
Sekcja L – Działalność związana z obsługą rynku nieruchomości
Sekcja M – Działalność profesjonalna, naukowa i techniczna
Sekcja N – Działalność w zakresie usług administrowania i działalność wspierająca
Sekcja O – Administracja publiczna i obrona narodowa; obowiązkowe zabezpieczenia społeczne
Sekcja P – Edukacja
Sekcja Q – Opieka zdrowotna i pomoc społeczna
Sekcja R – Działalność związana z kulturą, rozrywką i rekreacją
Sekcja S – Pozostała działalność usługowa
Sekcja T – Gospodarstwa domowe zatrudniające pracowników; gospodarstwa domowe produkujące wyroby i świadczące usługi na własne potrzeby
Sekcja U – Organizacje i zespoły eksterytorialne
"""

NCBIR_MATCHING = {
    "Biogospodarka rolno-spożywcza, leśno-drzewna i środowiskowa": ["A", "J", "M"],
    "Dostępność": ["C", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U"],
    "Edukacja i rozwój kompetencji": [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
    ],
    "Gospodarka o obiegu zamkniętym - woda, surowce kopalne, odpady": ["B", "E", "H", "J", "M"],
    "Inne": [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "R",
        "S",
        "T",
        "U",
    ],
    "Innowacyjne technologie i procesy przemysłowe": [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "H",
        "J",
        "K",
        "L",
        "M",
        "P",
        "Q",
    ],
    "Obronność i bezpieczeństwo": ["C", "J", "O", "U"],
    "Zdrowe społeczeństwo": ["J", "Q", "A"],
    "Zrównoważona energetyka": [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "R",
        "S",
        "T",
        "U",
    ],
}
