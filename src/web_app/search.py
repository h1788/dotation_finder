from flask import Blueprint, render_template, request

from .search_engine import search_for_dotation, search_for_dotations_new

auth = Blueprint("auth", __name__)


@auth.route("/search", methods=["POST"])
def search_post():
    nip = request.form.get("nip")
    typ = request.form.get("type")
    info = request.form.getlist("info")
    results, is_3w, company_data = search_for_dotation(nip, typ, info)
    if company_data is None:
        return render_template("error.html", nip=nip)
    else:
        return render_template(
            "search_results.html",
            nip=nip,
            results=results,
            is_3w=is_3w,
            typ=company_data.nazwa_firmy,
        )


@auth.route("/search_new", methods=["POST"])
def search_post_new():
    pkd = request.form.getlist("pkd")
    woj = request.form.get("woj")
    info = request.form.getlist("info")
    results, is_3w = search_for_dotations_new(pkd, info)
    return render_template("search_results_new.html", pkd=pkd, results=results, is_3w=is_3w)
