import html
import random
import re

import lxml.html
import pandas as pd
import requests


def ncbr_scraping():
    headers = {
        "authority": "www.gov.pl",
        "sec-ch-ua": '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
        "accept": "application/json, text/plain, */*",
        "sec-ch-ua-mobile": "?0",
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.54 Safari/537.36",
        "sec-ch-ua-platform": '"Linux"',
        "sec-fetch-site": "same-origin",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        "referer": "https://www.gov.pl/web/ncbr/platforma-konkursowa",
        "accept-language": "pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7",
    }

    params = (
        ("type", "NCBR"),
        ("language", "pl"),
        ("sort", "announcementDate,desc"),
        ("page", "1"),
        ("size", "200"),
    )

    sess = requests.Session()
    sess.get("https://www.gov.pl/api/data/competition-search")

    response = sess.get(
        "https://www.gov.pl/api/data/competition-search", headers=headers, params=params
    )
    if response.status_code == 200:
        jsonik = response.json()
        df = pd.DataFrame(jsonik["content"])

    old_cols = [
        "name",
        "path",
        "description",
        "budget",
        "status",
        "typesOfApplicant",
        "thematicAreas",
        "applicationsEndDate",
    ]

    df = df[old_cols]
    df["path"] = df["path"].apply(lambda x: str("https://www.gov.pl/" + x))
    # df['description'] = df['description'].apply(lambda x: )
    print(df["applicationsEndDate"].values)
    df["applicationsEndDate"] = df["applicationsEndDate"].apply(lambda x: pd.to_datetime(x))
    df["projectstart"] = df["applicationsEndDate"] + pd.offsets.DateOffset(years=1)
    categories = ["marketing", "badania_rozwój", "badania kliniczne", "ekspansja", "skalowalność"]

    empty_list = []
    for element in range(len(df)):
        empty_list.append(random.sample(categories, 2))
    df["target"] = empty_list
    df["returned"] = [True for i in range(len(df))]
    df["konsorcjum"] = [True if random.randint(x, x + 10) % 2 else False for x in range(len(df))]
    df["equity"] = [0 for i in range(len(df))]
    df["id"] = [i for i in range(len(df))]

    print(df.columns)

    new_cols = [
        "nazwa_dotacji",
        "url",
        "opis",
        "kwota",
        "otwarty",
        "kategoria",
        "rodzaj",
        "koniec_zgloszen",
        "poczatek_projektu",
        "cel",
        "zwrotna",
        "konsorcjum",
        "equity",
        "id",
    ]

    df.columns = new_cols

    df["otwarty"] = df["otwarty"].apply(
        lambda x: True if x == "APPLICATIONS_IN_PROGRESS" else False
    )

    df["opis"] = df["opis"].apply(lambda x: html.unescape(x).replace("<p>", "").replace("</p>", ""))

    df["rodzaj"] = df["rodzaj"].apply(
        lambda x: x.replace("healthy_society", "Zdrowe społeczeństwo")
        .replace("accessibility", "Dostępność")
        .replace("innovative_technologies", "Innowacyjne technologie i procesy przemysłowe")
        .replace("defence_and_security", "Obronność i bezpieczeństwo")
        .replace("others", "Inne")
        .replace("sustainable_energy", "Zrównoważona energetyka")
        .replace(
            "circular_economy", "Gospodarka o obiegu zamkniętym - woda, surowce kopalne, odpady"
        )
        .replace("agri_food", "Biogospodarka rolno-spożywcza, leśno-drzewna i środowiskowa")
        .replace("education", "Edukacja i rozwój kompetencji")
    )

    df["files"] = [get_files(x) for x in df["url"]]

    df["wklad_wlasny"] = [15 for x in range(len(df))]
    df.to_csv("src/datasets/dotations.csv")


def get_files(url):
    site = requests.get(url)
    if site.status_code == 200:
        content = lxml.html.fromstring(site.content.decode("utf-8"))
        list_of_files = []
        for parsed_content in content.xpath('//*[@id="main-content"]/div[1]/ul[2]/li/div/a'):
            href = str(parsed_content.attrib["href"])
            href = "https://www.gov.pl" + href
            parsed_content = str(parsed_content.text_content())
            parsed_content = parsed_content.split("\n")
            parsed_content = [content for content in parsed_content if content != ""]

            extension = parsed_content[1].split(".")[-1]
            list_of_files.append((href, parsed_content[0], extension))
        print(url, len(list_of_files))
        return list_of_files


if __name__ == "__main__":
    # ncbr_scraping()
    ncbr_scraping()
