import re
from dataclasses import dataclass

import lxml.html
import pandas as pd
import requests
import spacy

from src.web_app.read_company_data import read_company_data


def pkd_data_cleaner():
    cols = ["nazwa", "symbol", "category"]
    df = pd.read_csv("src/datasets/pkd2007.csv")

    w3_categories = ["A", "B", "C", "D", "E", "F", "H", "M"]

    only_full_pkd = (
        df[cols]
        .pipe(lambda df: df[df["symbol"].str.len() == 7])
        .pipe(
            lambda df: df.assign(
                w3=[True if element in w3_categories else False for element in df["category"]]
            )
        )
    )
    only_full_pkd["symbol"] = only_full_pkd["symbol"].str.replace(".", "")

    return only_full_pkd


# TODO: should work also at eng sites
def check_if_3W_in_website(url):
    if url == '':
        return False
    keywords = {"woda", "karbon", "węgiel", "wodór", "fuleren", "nanotechnologia", "grafen"}
    if 'http://' or 'https://' not in url:
        url = 'http://'+url
    site = requests.get(url, verify=False)
    if site.status_code == 200:
        content = lxml.html.fromstring(site.content.decode("utf-8"))
        parsed_content = str(content.xpath("/html/body")[0].text_content())
        parsed_content = re.sub(r"/[^a-zA-Z0-9]/g", "", parsed_content)
        parsed_content = re.sub("\n", " ", parsed_content)
        parsed_content = re.sub(" +", " ", parsed_content)

        # keeping only tagger component needed for lemmatization
        nlp = spacy.load("pl_core_news_sm", disable=["parser", "ner"])

        doc = nlp(parsed_content)
        words_lemmas_set = set([token.lemma_ for token in doc])

        if keywords.intersection(words_lemmas_set):
            return True
        else:
            return False


def check_if_pkd_in_our_list(pkds: pd.DataFrame, company_pkds: list):
    df = pkds[pkds["symbol"].isin(company_pkds)]
    true_or_false = dict(df["w3"].value_counts())
    
    if true_or_false.get(True, 0) > true_or_false.get(False, 0):
        return True
    else:
        return False


def is_a_company_3W_interested(pkds: pd.DataFrame, company_pkds, company_site):
    print(f"Company site: {company_site}")
    if check_if_3W_in_website(company_site) or check_if_pkd_in_our_list(pkds, company_pkds):
        return True
    else:
        return False



if __name__ == "__main__":
    pkds = pkd_data_cleaner()
    company_data = read_company_data(nip=8982250026)

    print(
        is_a_company_3W_interested(
            pkds,
            company_pkds=company_data.fiz_pkd_Kod,
            company_site=company_data.fiz_adresStronyinternetowej,
        )
    )
    print(
        is_a_company_3W_interested(
            pkds, company_pkds=company_data.fiz_pkd_Kod, company_site="https://www.jsw.pl/"
        )
    )
    print(
        is_a_company_3W_interested(
            pkds, company_pkds=["0510Z"], company_site=company_data.fiz_adresStronyinternetowej
        )
    )
