from dataclasses import dataclass
from gusregon import GUS

import pandas as pd


@dataclass
class CompanyData:
    nazwa_firmy: str
    nip: int
    fiz_adSiedzWojewodztwo_Symbol: int
    fiz_adresStronyinternetowej: str
    fiz_pkd_Kod: list
    fiz_pkd_type: list


def pkd_data_cleaner():
    cols = ["nazwa", "symbol", "category"]
    df = pd.read_csv("src/datasets/pkd2007.csv")

    w3_categories = ["A", "B", "C", "D", "E", "F", "H", "M"]

    only_full_pkd = (
        df[cols]
        .pipe(lambda df: df[df["symbol"].str.len() == 7])
        .pipe(
            lambda df: df.assign(
                w3=[True if element in w3_categories else False for element in df["category"]]
            )
        )
    )
    only_full_pkd["symbol"] = only_full_pkd["symbol"].str.replace(".", "")

    return only_full_pkd


def read_company_data(nip: int, from_api = True):

    if from_api:

        gus = GUS(api_key='d685be40bfc14666b22c')
        result = gus.search(nip=nip)

        if result:

            pkds = gus.get_pkd(nip=nip)
            pkds_codes = [pkd['code'] for pkd in pkds]


            data_pkd = pkd_data_cleaner()
            pkds_filtered = data_pkd[data_pkd['symbol'].isin(pkds_codes)]
            categories = set(pkds_filtered.category)

            company_data = CompanyData(
                nazwa_firmy = result['nazwa'],
                nip = nip,
                fiz_adSiedzWojewodztwo_Symbol = int(result['adsiedzwojewodztwo_symbol']),
                fiz_adresStronyinternetowej = result['adresstronyinternetowej'],
                fiz_pkd_Kod = pkds_codes,
                fiz_pkd_type = categories
            )
            return company_data

        return None

    else:
        df = pd.read_csv("src/datasets/example_data_from_api.csv")
        row = df[df["nip"] == nip].to_records(index=False)
        if len(row) == 0:
            return None
        else:
            company_data = CompanyData(
                row["nazwa_firmy"][-1],
                row["nip"][-1],
                row["fiz_adSiedzWojewodztwo_Symbol"][-1],
                row["fiz_adresStronyinternetowej"][-1],
                row["fiz_pkd_Kod"][-1].replace("[", "").replace("]", "").split(","),
                row["fiz_pkd_type"][-1].replace("[", "").replace("]", "").split(","),
            )
            return company_data


if __name__ == "__main__":
    read_company_data(8982250026)