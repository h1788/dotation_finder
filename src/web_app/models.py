from flask_login import UserMixin

from . import db


class Dotation(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nazwa = db.Column(db.String(100))
    kwota = db.Column(db.Float)
    czy_otwarty = db.Column(db.Boolean)
    do_kiedy = db.Column(db.Date)
    czy_zwrotna = db.Column(db.Boolean)
    equity_max = db.Column(db.Float)
    czy_konsorcjum = db.Column(db.Boolean)
    kategoria = db.Column(db.String(500))
    rodzaj = db.Column(db.String(100))
    link_do_strony = db.Column(db.String(200))
