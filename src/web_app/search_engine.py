import sqlite3

from src.web_app.check_3W import is_a_company_3W_interested, pkd_data_cleaner
from src.web_app.matching import NCBIR_MATCHING
from src.web_app.read_company_data import read_company_data
from src.web_app.read_dotations_data import read_dotations_data


def create_connection(db_file):
    """create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Exception as e:
        print(e)

    return conn


def search_for_dotation(nip, typ, info):

    # rows = [[0, "ERA PerMed (V konkurs 2022)", 2771128.95, ["badania_kliniczne", "badania_rozwoj"], True, "17-02-2022", "01-01-2023", False, 0.0, False, ["zdrowe_spoleczenstwo"],
    #      ["duza_firma", "jednostka_naukowa",  "konsorcjum", "mikrofirma", "uczelnia"], "https://trasee.io"]]

    try:
        nip = int(nip)
    except ValueError:
        print('NIP in incorrect format. Please use only digits')
        nip = 0

    pkds = pkd_data_cleaner()
    company_data = read_company_data(nip=nip)

    if company_data:
        categories = []
        for key, value in NCBIR_MATCHING.items():
            for element in company_data.fiz_pkd_type:
                if element in value:
                    categories.append(key)
                    break

        rows = read_dotations_data(categories, typ)

        is_3w = is_a_company_3W_interested(
            pkds,
            company_pkds=company_data.fiz_pkd_Kod,
            company_site=company_data.fiz_adresStronyinternetowej,
        )

        return rows, is_3w, company_data
    
    else:
        return None, False, company_data



def search_for_dotations_new(pkd, info):

    categories = []
    for key, value in NCBIR_MATCHING.items():
        for element in pkd:
            if element in value:
                categories.append(key)
                break

    rows = read_dotations_data(categories, "micro")

    is_3w = False

    return rows, is_3w
